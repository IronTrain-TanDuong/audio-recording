package com.example.mytest;

import android.support.v7.app.AppCompatActivity;
import android.media.AudioManager;
import android.speech.SpeechRecognizer;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.content.Intent;
import android.content.Context;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import com.example.mytest.MainActivity;


import android.util.Log;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.widget.Toast;
import android.os.Environment;
import android.net.Uri;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.FileOutputStream;
import org.json.JSONArray;
import java.util.Date;
import java.io.OutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.util.Locale;


import android.support.v7.app.AppCompatActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.ActivityCompat;
import android.app.Activity;


public class SpeechRecognizerManager extends AppCompatActivity {

    private static final String LOG_TAG = SpeechRecognizer.class.getSimpleName();
    private static int REQUEST_CODE = 1001;

    //private CallbackContext callbackContext;


    protected AudioManager mAudioManager;
    protected SpeechRecognizer mSpeechRecognizer;
    protected Intent mSpeechRecognizerIntent;

    protected boolean mIsListening;
    private boolean mIsStreamSolo;


    private boolean mMute = true;

    private final static String TAG = "SpeechRecognizerManager";

    private onResultsReady mListener;

    Uri audioUri;
    MediaPlayer mPlayer;
    private Visualizer mVisualizer;
    String upLoadServerUri = "http://mp3.3030class.com/lib/postamr.php";
    String selectedPath="";
    int serverResponseCode=0;

    //@Override
    public boolean execute(String action, JSONArray args) {
        Boolean isValidAction = true;
        // Action selector
        if ("startRecognize".equals(action)) {
            // recognize speech
            startSpeechRecognitionActivity();
        }

        return isValidAction;

    }
    public void startSpeechRecognitionActivity() {
               // Create the intent and set parameters
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra("android.speech.extra.GET_AUDIO", true);
        intent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");


        startActivityForResult(intent, REQUEST_CODE);
    }




    /**
     * Handle the results from the recognition activity.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
        // Save audio to file

        Uri audioUri = data.getData();
        ContentResolver contentResolver = getContentResolver();
        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            inputStream = contentResolver.openInputStream(audioUri);
            outputStream = null;
            final File recordFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/test");
            if (!recordFile.exists()) {
                recordFile.mkdir();
            }
            String FileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".amr";
            outputStream = new FileOutputStream(recordFile + "/" + FileName);
            // Transfer bytes from in to out
            int read = 0;
            byte[] buf = new byte[1024];

            while ((read = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, read);
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {

                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
        }
        AudioManager audiomanager =(AudioManager) getSystemService(Context.AUDIO_SERVICE);

            int currentVolumn = audiomanager.getStreamVolume(AudioManager.STREAM_MUSIC);
            audiomanager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolumn, 0);
            mPlayer = new MediaPlayer();
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);


            android.media.audiofx.Visualizer.OnDataCaptureListener ondatacapturelistener;

            try
            {
                mPlayer.setDataSource(this, audioUri);
            }
            catch (Exception exception)
            {
                irontrain.getAppview().post(new Runnable() {
                    public void run()
                    {
                        irontrain.getAppview().loadUrl("javascript:drawRecSpectrum();");
                    }
                });
            }

            try
            {
                mPlayer.prepare();
            }
            catch (IllegalStateException illegalstateexception) { }
            catch (IOException ioexception) { }

            mPlayer.start();
            mVisualizer = new Visualizer(mPlayer.getAudioSessionId());
            mVisualizer.setCaptureSize(2048);

            ondatacapturelistener = new android.media.audiofx.Visualizer.OnDataCaptureListener() {
                public void onFftDataCapture(Visualizer visualizer, byte abyte0[], int k)
                {
                    float f = 0.0F;
                    float f1 = abyte0.length;
                    for (int l = 0; (float)l < f1; l++)
                    {
                        f = Math.max(f, abyte0[l]);
                    }

                    final float max2 = f;
                    irontrain.getAppview().post(
                            new Runnable() {
                                public void run()
                                {
                                    irontrain.getAppview().loadUrl("javascript:inputRecSpectrum("+max2+");");
                                }
                            });
                }

                public void onWaveFormDataCapture(Visualizer visualizer, byte abyte0[], int k)
                {
                }
            };

            mVisualizer.setDataCaptureListener(ondatacapturelistener, Visualizer.getMaxCaptureRate(), true, true);
            mVisualizer.setEnabled(true);

            mPlayer.setOnCompletionListener(new android.media.MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaplayer)
                {
                    mVisualizer.setEnabled(false);
                    mediaplayer.stop();
                    mediaplayer.release();
                    irontrain.getAppview().post(new Runnable() {
                        public void run()
                        {
                            irontrain.getAppview().loadUrl("javascript:drawRecSpectrum();");
                        }
                    });
                }
            });

            // Fill the list view with the strings the recognizer thought it could have heard
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

            returnSpeechResults(matches);
        }
        else {
            // Failure - Let the caller know
            //this.callbackContext.error(Integer.toString(resultCode));
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void returnSpeechResults(ArrayList<String> matches) {
        JSONArray jsonMatches = new JSONArray(matches);
        // this.callbackContext.success(jsonMatches);

    }

    private void listenAgain()
    {
        if(mIsListening) {
            mIsListening = false;
            mSpeechRecognizer.cancel();
            startListening();
        }
    }


    private void startListening()
    {
        if(!mIsListening)
        {
            mIsListening = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                // turn off beep sound
                if (!mIsStreamSolo && mMute) {
                    mAudioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
                    mAudioManager.setStreamMute(AudioManager.STREAM_ALARM, true);
                    mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                    mAudioManager.setStreamMute(AudioManager.STREAM_RING, true);
                    mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
                    mIsStreamSolo = true;
                }
            }
            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
        }
    }

    public void destroy()
    {
        mIsListening=false;
        if (!mIsStreamSolo) {
            mAudioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
            mAudioManager.setStreamMute(AudioManager.STREAM_ALARM, false);
            mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
            mAudioManager.setStreamMute(AudioManager.STREAM_RING, false);
            mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
            mIsStreamSolo = true;
        }
        Log.d(TAG, "onDestroy");
        if (mSpeechRecognizer != null)
        {
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.destroy();
            mSpeechRecognizer=null;
        }

    }

    protected class SpeechRecognitionListener implements RecognitionListener
    {

        @Override
        public void onBeginningOfSpeech() {}

        @Override
        public void onBufferReceived(byte[] buffer)
        {

        }

        @Override
        public void onEndOfSpeech()
        {}

        @Override
        public synchronized void onError(int error)
        {

            if(error==SpeechRecognizer.ERROR_RECOGNIZER_BUSY)
            {
                if(mListener!=null) {
                    ArrayList<String> errorList=new ArrayList<String>(1);
                    errorList.add("ERROR RECOGNIZER BUSY");
                    if(mListener!=null)
                        mListener.onResults(errorList);
                }
                return;
            }

            if(error==SpeechRecognizer.ERROR_NO_MATCH)
            {
                if(mListener!=null)
                    mListener.onResults(null);
            }

            if(error==SpeechRecognizer.ERROR_NETWORK)
            {
                ArrayList<String> errorList=new ArrayList<String>(1);
                errorList.add("STOPPED LISTENING");
                if(mListener!=null)
                    mListener.onResults(errorList);
            }
            Log.d(TAG, "error = " + error);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    listenAgain();
                }
            },100);


        }

        @Override
        public void onEvent(int eventType, Bundle params)
        {

        }

        @Override
        public void onPartialResults(Bundle partialResults)
        {

        }

        @Override
        public void onReadyForSpeech(Bundle params) {}

        @Override
        public void onResults(Bundle results)
        {
            if(results!=null && mListener!=null)
                mListener.onResults(results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION));
            listenAgain();

        }

        @Override
        public void onRmsChanged(float rmsdB) {}

    }

    public boolean ismIsListening() {
        return mIsListening;
    }


    public interface onResultsReady
    {
        public void onResults(ArrayList<String> results);
    }

    public void mute(boolean mute)
    {
        mMute=mute;
    }

    public boolean isInMuteMode()
    {
        return mMute;
    }


}
