package com.example.mytest;

import  android.webkit.WebView;

public class irontrain {
    static WebView appview;
    static String cid;
    static String nowno;
    static String selbookid;
    static String selday;
    static String userclass;
    static String userid;
    static String username;
    static String leveltestid;


    public static WebView getAppview() {
        return appview;
    }

    public static String getCid() {
        return cid;
    }

    public static String getNowno() {
        return nowno;
    }

    public static String getSelbookid() {
        return selbookid;
    }

    public static String getSelday() {
        return selday;
    }

    public static String getUserclass() {
        return userclass;
    }

    public static String getUserid() {
        return userid;
    }

    public static String getUsername() {
        return username;
    }

    public static void setCid(String paramString) {
        cid = paramString;
    }

    public static void setNowno(String paramString) {
        nowno = paramString;
    }

    public static String getLeveltestid() {
        return leveltestid;
    }
}
